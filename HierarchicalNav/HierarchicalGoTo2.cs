﻿using System;

using Xamarin.Forms;

namespace HierarchicalNav
{
    public class HierarchicalGoTo2 : ContentPage
    {
        public HierarchicalGoTo2()
        {
            Content = new StackLayout
            {
                Children = {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}

