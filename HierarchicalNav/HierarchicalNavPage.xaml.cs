﻿using Xamarin.Forms;

namespace HierarchicalNav
{
    public partial class HierarchicalNavPage : ContentPage
    {
        public HierarchicalNavPage()
        {
            InitializeComponent();
        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync (new HierarchicalGoTo());
        }
    }
}
